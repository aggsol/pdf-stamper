using System;
using pdfstamper;
using Xunit;

namespace UnitTests
{
    public class PageRangeTest
    {
        [Fact]
        public void NullInput()
        {
            var pageRange = new PageRange();
            Assert.Throws<ArgumentNullException>(() => pageRange.Parse(null));
        }

        [Fact]
        public void ValuesMixed()
        {
            var pageRange = new PageRange();
            pageRange.Parse("[100-102,199,200-202]");
            
            Assert.True(pageRange.IsInRange(100));
            Assert.True(pageRange.IsInRange(101));
            Assert.True(pageRange.IsInRange(101));
            
            Assert.True(pageRange.IsInRange(199));
            
            Assert.True(pageRange.IsInRange(200));
            Assert.True(pageRange.IsInRange(201));
            Assert.True(pageRange.IsInRange(201));
            
            Assert.False(pageRange.IsInRange(198));
            Assert.False(pageRange.IsInRange(99));
            Assert.False(pageRange.IsInRange(205));
        }
        
        [Fact]
        public void ValuesRanges()
        {
            var pageRange = new PageRange();
            pageRange.Parse("[10-12]");

            Assert.True(pageRange.IsInRange(10));
            Assert.True(pageRange.IsInRange(11));
            Assert.True(pageRange.IsInRange(12));

            Assert.False(pageRange.IsInRange(9));
            Assert.False(pageRange.IsInRange(13));
            Assert.False(pageRange.IsInRange(100));
            Assert.False(pageRange.IsInRange(120));
        }

        [Fact]
        public void ValuesSingles()
        {
            var pageRange = new PageRange();
            pageRange.Parse("[2]");

            Assert.True(pageRange.IsInRange(2));
            Assert.False(pageRange.IsInRange(22));

            pageRange.Parse("[33,333,3333]");
            Assert.True(pageRange.IsInRange(33));
            Assert.True(pageRange.IsInRange(333));
            Assert.True(pageRange.IsInRange(3333));
            Assert.False(pageRange.IsInRange(3));

        }

        [Theory]
        [InlineData("[3]")]
        [InlineData("[4-5]")]
        [InlineData("[4,8-9]")]
        [InlineData("[6,7,9]")]
        [InlineData("[12-14,66]")]
        [InlineData("[10-12,20-22]")]
        public void RangeFormat(string input)
        {
            var pageRange = new PageRange();
            pageRange.Parse(input);
        }

        [Theory]
        [InlineData("[4")]
        [InlineData("12,23]")]
        [InlineData("12-34")]
        public void BadFormat(string input)
        {
            var pageRange = new PageRange();
            Assert.Throws<ArgumentException>(() => pageRange.Parse(input));
        }
    }
}