/*
    Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
    This file is part of pdfstamper.

    pdfstamper is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;

namespace pdfstamper
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Options
    {
        [Option('i', "input", Required = true, HelpText = "Input file to be processed.")]
        public string InputFile { get; set; }

        [Option('o', "output", Required = false, HelpText = "Output file. If not set then <input-file>.stamped.pdf will be used.")]
        public string OutputFile { get; set; }
        
        [Option('w', "watermark", 
            Required = false,
            HelpText = "Stamp the given watermark text.")]
        public string Watermark { get; set; }
        
        [Option('n', "page-numbers", 
            Required = false,
            HelpText = "Stamp page numbers. Use @page for the page number, @count for the page count.")]
        public string PageNumbers { get; set; }
        
        [Option('d',"dices", 
            Required = false,
            HelpText = "Stamp n dices on the page bottom")]
        public int? NumDice { get; set; }
        
        [Option('l', "limit-dice", Required = false, HelpText = "Never roll a 6 after a 6 or a 1 after a 1.")]
        public bool LimitDice { get; set; }
        
        [Option('r', "page-range", Required = false, HelpText = "Stamp on a range of pages. Format: [a,b-c,d] where a > b > c > d")]
        public string PageRange { get; set; }

        [Option('s', "page-select", Required = false, HelpText = "Limit stamping on 'Even' or 'Odd' pages.")]
        public PageSelect PageSelector { get; set; }

        [Option('c', "color", Required = false, HelpText = "Set the color in hex RRGGBBAA format. Default: 000000FF")]
        public string Color { get; set; }
            
        [Option('t', "tiny-text", Required = false, HelpText = "Stamp a tiny colored text bar to page top")]
        public string TinyTextBar { get; set; }

        [Option('p', "permit-modifications", Required = false, Default = false, HelpText = "Permit following modifications of PDF (weak)")]
        public bool PermitModifications { get; set; }

        [Usage(ApplicationAlias = "pdf-stamper")]
        public static IEnumerable<Example> Examples
        {
            get
            {
                return new List<Example>() {
                    new Example("Stamp a light red top secret watermark on every page", new Options
                    {
                        InputFile = "your-file.pdf", Watermark = "Top Secret",
                        OutputFile = "stamped.pdf", Color = "FF000040"
                    }),
                    new Example("Stamp page numbers on every page", new Options
                    {
                        InputFile = "your-file.pdf", PageNumbers = "Page @page of @count",
                        OutputFile = "stamped.pdf"
                    }),
                    new Example("Stamp two dices on the pages 4 to 200", new Options
                    {
                        InputFile = "your-file.pdf", NumDice = 2,
                        PageRange = "[2-200]",
                        OutputFile = "stamped.pdf"
                    })
                };
            }
        }

    }
}