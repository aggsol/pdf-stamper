/*
    Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
    This file is part of pdfstamper.

    pdfstamper is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf;

namespace pdfstamper
{
    public class Stamper : BaseStamper
    {
        private readonly PdfDocument _pdfDocument;
        private readonly XColor _color;
        
        public Stamper(PdfDocument pdfDocument, PageRange pageRange,  PageSelect pgSelect, XColor color)
        {
            _pdfDocument = pdfDocument;
            _pageRange = pageRange;
            _color = color;
            _pageSelect = pgSelect;
        }
        
        public void StampPageNumbers(string format)
        {
            var font = new XFont("Arial", 8);
            XBrush brush = new XSolidBrush(_color);

            string noPages = _pdfDocument.Pages.Count.ToString();
            
            format = format.Replace("@count", $"{noPages}");
            
            for (int i = 0; i < _pdfDocument.PageCount; ++i)
            {
                if (IsPageInRange(i + 1) == false) continue;
                if (IsPageSelected(i + 1) == false) continue;
                
                var page = _pdfDocument.Pages[i];
                XRect layoutRectangle = new XRect(0, page.Height-font.Height, page.Width, font.Height);

                using (XGraphics gfx = XGraphics.FromPdfPage(page))
                {
                    var pageNum = format.Replace("@page", $"{i + 1}");
                    gfx.DrawString(
                        pageNum,
                        font,
                        brush,
                        layoutRectangle,
                        XStringFormats.Center);
                }
            }
        }

        public void StampWatermark(string watermark)
        {
            var font = new XFont("Arial", 120, XFontStyle.Bold);
            XBrush brush = new XSolidBrush(_color);
            var format = new XStringFormat {Alignment = XStringAlignment.Near, LineAlignment = XLineAlignment.Near};

            for (int i = 0; i < _pdfDocument.PageCount; ++i)
            {
                if (IsPageInRange(i + 1) == false) continue;
                if (IsPageSelected(i + 1) == false) continue;
                
                var page = _pdfDocument.Pages[i];
                using (var gfx = XGraphics.FromPdfPage(page, XGraphicsPdfPageOptions.Append))
                {
                    var size = gfx.MeasureString(watermark, font);
                    gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                    gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                    gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                    gfx.DrawString(watermark, font, brush,
                        new XPoint((page.Width - size.Width) / 2, (page.Height - size.Height) / 2),
                        format);
                }
            }
        }

        public void StampTinyText(string txt)
        {
            var font = new XFont("Arial", 2);
            XBrush brush = new XSolidBrush(_color);

            string noPages = _pdfDocument.Pages.Count.ToString();
            
            
            for (int i = 0; i < _pdfDocument.PageCount; ++i)
            {
                if (IsPageInRange(i + 1) == false) continue;
                if (IsPageSelected(i + 1) == false) continue;
                
                var page = _pdfDocument.Pages[i];
                XRect layoutRectangle = new XRect(0, font.Height, page.Width, font.Height);

                using (XGraphics gfx = XGraphics.FromPdfPage(page))
                {
                    gfx.DrawRectangle(brush, layoutRectangle);
                    
                    gfx.DrawString(
                        txt,
                        font,
                        brush,
                        layoutRectangle,
                        XStringFormats.BottomRight);
                }
            }
        }
    }
}