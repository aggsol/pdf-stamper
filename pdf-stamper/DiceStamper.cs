/*
    Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
    This file is part of pdfstamper.

    pdfstamper is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf;

namespace pdfstamper
{
    public class DiceStamper : BaseStamper
    {
        private readonly PdfDocument _pdfDocument;
        private readonly int _numDice;
        private readonly Dictionary<int, XImage> _diceImages;
        
        private readonly Random _rand;
        private int _lastRoll;

        public bool LimitDice { get; set; } = false;

        public DiceStamper(PdfDocument pdfDocument, int numDice, PageRange pageRange, PageSelect pgSelect, int seed = 1337)
        {
            _pdfDocument = pdfDocument;
            _numDice = numDice;
            _pageRange = pageRange;
            _pageSelect = pgSelect;
            _rand = new Random(1337);

            var assembly = typeof(Stamper).GetTypeInfo().Assembly;
            
            _diceImages = new Dictionary<int, XImage>();
            for (int i = 1; i <= 6; ++i)
            {
                var stream = assembly.GetManifestResourceStream($"pdfstamper.images.inverted-dice-{i}.png");
                var image = XImage.FromStream(() => stream);
                image.Interpolate = true;
                _diceImages[i] = image;
            }
        }

        public void StampDice()
        {
            int imageSize = 30;
            int offsetY = 60;
            int offsetX = (_numDice * imageSize) / 2;
            
            for (int i = 0; i < _pdfDocument.Pages.Count; ++i)
            {
                if (IsPageInRange(i + 1) == false) continue;
                if (IsPageSelected(i + 1) == false) continue;
                
                var page = _pdfDocument.Pages[i];

                var halfPageWidth = page.Width / 2;
                
                for (int j = 0; j < _numDice; ++j)
                {
                    XRect layout = new XRect(
                        halfPageWidth - offsetX + (j*imageSize), 
                        page.Height-offsetY, 
                        imageSize, 
                        imageSize
                    );

                    int index = RollDice();
                    var image = _diceImages[index];
                    StampImage(page, image, layout);
                }
            }
        }
        
        private void StampImage(PdfPage page, XImage image, XRect rect)
        {
            using (var gfx = XGraphics.FromPdfPage(page))
            {
                gfx.DrawImage(image, rect);
            }
        }

        private int RollDice()
        {
            if (LimitDice == false)
            {
                return _rand.Next(1, 6);
            }

            bool found = false;
            do
            {
                var roll = _rand.Next(1, 6);
                // Do not roll a 6 after a 6
                // Do not roll a 1 after a 1
                if (roll != 1 && roll != 6)
                {
                    _lastRoll = roll;
                    found = true;
                }
                
                Debug.Assert(roll == 6 || roll == 1);
                if(roll != _lastRoll)
                {
                    _lastRoll = roll;
                    found = true;
                }
                    

            } while (found);

            Debug.Assert(_lastRoll >= 1 && _lastRoll <= 6);
            return _lastRoll;
        }
    }
}