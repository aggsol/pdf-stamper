/*
    Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
    This file is part of pdfstamper.

    pdfstamper is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;

namespace pdfstamper
{
    /// <summary>
    /// Base class for all stampers with basic functions
    /// </summary>
    public class BaseStamper
    {
        protected PageRange _pageRange;
        protected PageSelect _pageSelect;

        protected bool IsPageSelected(int pageNum)
        {
            switch (_pageSelect)
            {
                case PageSelect.All:
                    return true;
                    
                case PageSelect.Even:
                    return pageNum % 2 == 0;
                    
                case PageSelect.Odd:
                    return pageNum % 2 == 1;
                    
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        protected bool IsPageInRange(int pageNum)
        {
            if (_pageRange == null) return true;

            return _pageRange.IsInRange(pageNum);
        }
    }
}