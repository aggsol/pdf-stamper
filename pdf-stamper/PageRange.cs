/*
    Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
    This file is part of pdfstamper.

    pdfstamper is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace pdfstamper
{
    public class PageRange
    {
        internal class RangeEntry
        {
            public enum EntryType
            {
                None,
                Single,
                Range,
                FromStart,
                ToEnd
            }

            public EntryType Type { get; set; } = EntryType.None;
            public int From { get; set; } = -1;
            public int To { get; set; } = -1;
        }

        public void Parse(string input)
        {

            if (input == null || string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentNullException(nameof(input));
            }

            if (input.Length <= 2 || input[0] != '[' || input.Last() != ']')
            {
                throw new ArgumentException("Invalid range format. Expected '[a,b-c,d] where a > b > c > d");
            }

            _ranges = new Dictionary<int, RangeEntry>();

            State state = State.Start;
            string value = "";
            int number = -1;
            
            for (int i = 0; i < input.Length; ++i)
            {
                char c = input[i];

                switch (state)
                {
                    case State.Start:
                        if (c != '[')
                        {
                            throw new ArgumentException("Page range must start with a '['");
                        }

                        state = State.Single;
                        break;
                    case State.Single:
                        if (c == ',' || c == ']')
                        {
                            bool result = int.TryParse(value, out number);
                            Debug.Assert(result);
                            var entry = new RangeEntry
                                {From = number, To = number, Type = RangeEntry.EntryType.Single};
                            _ranges[number] = entry;
                            
                            value = "";
                            number = -1;
                        }
                        else if (c == '-')
                        {
                            bool result =  int.TryParse(value, out number);
                            Debug.Assert(result);
                            var entry = new RangeEntry
                                {From = number, Type = RangeEntry.EntryType.Range};
                            _ranges[number] = entry;
                            state = State.Range;
                            // Tricky; keep the number
                            value = "";
                        }
                        else if (Char.IsDigit(c) == false)
                        {
                            throw new ArgumentException("Page range expected a digit");
                        }
                        else
                        {
                            value += c;
                        }
                        break;
                    case State.Range:
                        if (c == ',' || c == ']')
                        {
                            var entry = _ranges[number];
                            bool result = int.TryParse(value, out number);
                            Debug.Assert(result);
                            entry.To = number;

                            if (entry.From >= entry.To)
                            {
                                throw new ArgumentException($"Invalid range from {entry.From} to {entry.To}");
                            }

                            value = "";
                            number = -1;
                            state = State.Single;
                        }
                        else if (Char.IsDigit(c) == false)
                        {
                            throw new ArgumentException("Page range expected a digit");
                        }
                        else
                        {
                            value += c;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
            }
        }

        public bool IsInRange(int pageNum)
        {
            foreach (var rangeEntry in _ranges)
            {
                if (rangeEntry.Value.From > pageNum)
                {
                    return false;
                }

                if (rangeEntry.Value.From <= pageNum && rangeEntry.Value.To >= pageNum)
                {
                    return true;
                }
            }

            return false;
        }

        private enum State
        {
            Start,
            Single,
            Range
        }

        private Dictionary<int, RangeEntry> _ranges;
    }
}