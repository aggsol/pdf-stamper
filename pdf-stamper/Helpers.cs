using System;

namespace pdfstamper
{
    public static class Helpers
    {
        /// <summary>
        /// Taken from SO
        /// https://stackoverflow.com/questions/311165/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-and-vice-versa/26304129#26304129
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] FromHexToByteArray(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentNullException(nameof(input));
            }

            if (input.Length % 2 == 1)
            {
                throw new ArgumentOutOfRangeException(nameof(input), "Two digits per byte are expected");
            }
            
            var outputLength = input.Length / 2;
            var output = new byte[outputLength];
            for (var i = 0; i < outputLength; i++)
            {
                output[i] = Convert.ToByte(input.Substring(i * 2, 2), 16);
            }

            return output;
        }
    }
}