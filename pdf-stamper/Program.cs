﻿/*
    Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
    This file is part of pdfstamper.

    pdfstamper is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Diagnostics;
using System.IO;
using CommandLine;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf.IO;

namespace pdfstamper
{
    class Program
    {
        static int Main(string[] args)
        {
            var parser = new Parser(x => x.HelpWriter = Console.Out);

            parser.ParseArguments<Options>(args)
                .WithParsed(o => RunOptionsAndReturnExitCode(o));
            
            return 0;
        }
        static int RunOptionsAndReturnExitCode(Options options)
        {
            if (!File.Exists(options.InputFile))
            {
                Console.Error.WriteLine($"File: {options.InputFile} does not exist");
                return 100;
            }

            if (string.IsNullOrWhiteSpace(options.OutputFile))
            {
                options.OutputFile = Path.ChangeExtension(options.InputFile, "stamped.pdf");
            }

            if (string.IsNullOrEmpty(options.Color))
            {
                options.Color = "000000FF";
            }

            if (options.Color.Length != 8)
            {
                Console.Error.WriteLine($"Invalid color: {options.Color}. Expected RRGGBBAA format.");
                return 101;
            }

            var rrggbbaa = Helpers.FromHexToByteArray(options.Color);
            Debug.Assert(rrggbbaa.Length == 4);

            var color = XColor.FromArgb(rrggbbaa[3], rrggbbaa[0], rrggbbaa[1], rrggbbaa[2]);
            
            PageRange pageRange = null;
            if (string.IsNullOrWhiteSpace(options.PageRange) == false)
            {
                pageRange = new PageRange();
                pageRange.Parse(options.PageRange);
            }
            
            var pdf = PdfReader.Open(options.InputFile, PdfDocumentOpenMode.Modify);

            var stamper = new Stamper(pdf, pageRange, options.PageSelector, color);

            if (!string.IsNullOrWhiteSpace(options.Watermark))
            {
                stamper.StampWatermark(options.Watermark);
            }

            if (!string.IsNullOrWhiteSpace(options.PageNumbers))
            {
                stamper.StampPageNumbers(options.PageNumbers);
            }

            if (!string.IsNullOrWhiteSpace(options.TinyTextBar))
            {
                stamper.StampTinyText(options.TinyTextBar);
            }
            
            if (options.NumDice.HasValue)
            {
                var diceStamper = new DiceStamper(pdf, options.NumDice.Value, pageRange, options.PageSelector);
                diceStamper.LimitDice = options.LimitDice;
                diceStamper.StampDice();
            }
            
            var settings = pdf.SecuritySettings;
            settings.PermitAnnotations = options.PermitModifications;
            settings.PermitAssembleDocument = options.PermitModifications;
            settings.PermitFormsFill = options.PermitModifications;
            settings.PermitModifyDocument = options.PermitModifications;

            pdf.Save(options.OutputFile);            
            
            return 0;
        }

    }
}
