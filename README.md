# pdfstamper v1.1.0

Modify existing PDF files easily.

* Stamp watermark text for drafts or top secret documents
* Stamp page numbers
* Stamp random dices
* Stamp tiny text bar

## Install

Download the current version for Ubuntu and Windows here:

https://gitlab.com/aggsol/pdf-stamper/tree/master/releases

## Options

```
  -i, --input           Required. Input file to be processed.

  -o, --output          Output file. If not set then <input-file>.stamped.pdf will be used.

  -w, --watermark       Stamp the given watermark text.

  -n, --page-numbers    Stamp page numbers. Use @page for the page number, @count for the page count.

  -d, --dices           Stamp n dices on the page bottom

  -l, --limit-dice      Never roll a 6 after a 6 or a 1 after a 1.

  -r, --page-range      Stamp on a range of pages. Format: [a,b-c,d] where a > b > c > d

  -s, --page-select     Limit stamping on 'Even' or 'Odd' pages.

  -c, --color           Set the color in hex RRGGBBAA format. Default: 000000FF

  -t, --tiny-text       Stamp a tiny colored text bar to page top

  -p, --permit-modifications    (Default: false) Permit following modifications of PDF (weak)

  --help                Display this help screen.

  --version             Display version information.


```

## Examples
 
Stamp page numbers on every page:

```sh
$ pdf-stamper --input your-file.pdf --page-numbers "Page @page of @count" --output stamped.pdf
```
Stamp two dices on the pages 4 to 200:

```sh
$ pdf-stamper --dices 2 --input your-file.pdf --output stamped.pdf --page-range [2-200]
```
Stamp a top secret watermark on every page

```sh
$ pdf-stamper --input your-file.pdf --output stamped.pdf --watermark "Top Secret"
```

## Licenses

**pdf-stamper is GPLv3**

### Dependencies

* [PdfSharpCore is MIT licensed](https://github.com/ststeiger/PdfSharpCore) is MIT licensed
* [Command Line Parser Library ](https://github.com/commandlineparser/commandline) is MIT licensed

### Icons

* [Inverted dice icons by Skoll under CC BY 3.0](https://game-icons.net)
* [Wax seal icon by Lorc under CC BY 3.0](https://game-icons.net)

